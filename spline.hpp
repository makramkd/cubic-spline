//
// Created by Makram Kamaleddine on 12/6/15.
//

#ifndef CSPLINE_SPLINE_HPP
#define CSPLINE_SPLINE_HPP

#include <vector>
#include <fstream>

namespace sp {

    /**
     * A POD that holds the coefficients of a cubic polynomial.
     */
    template<typename Float>
    struct cubic_piece {
        Float a, b, c, d;
        Float x; // leftmost point at which the piece is defined
    };

    template<typename Float>
    std::vector<cubic_piece<Float>> spline(const std::vector<Float>& xvalues,
                                           const std::vector<Float>& yvalues)
    {
        auto n = xvalues.size() - 1;
        std::vector<Float> a(yvalues.begin(), yvalues.end()), b(n),
                d(n), h(n), c(n + 1), l(n + 1), mu(n + 1), z(n + 1), alpha(n);
        l[0] = 1;
        mu[0] = z[0] = 0;
        std::vector<cubic_piece<Float>> result;

        for (auto i = 0; i < n; ++i) {
            h[i] = xvalues[i + 1] - xvalues[i];
        }

        for (auto i = 1; i < n; ++i) {
            alpha[i] = ((3 * (a[i + 1] - a[i])) / h[i]) - 3 * (a[i] - a[i - 1]) / h[i - 1];
        }

        for (auto i = 1; i < n; ++i)
        {
            l[i] = 2 * (xvalues[i + 1] - xvalues[i - 1]) - h[i - 1] * mu[i - 1];
            mu[i] = h[i] / l[i];
            z[i] = (alpha[i] - h[i - 1] * z[i - 1]) / l[i];
        }

        l[n] = 1;
        z[n] = c[n] = 0;

        for (int j = n - 1; j >= 0; --j) {
            c[j] = z[j] - mu[j] * c[j + 1];
            b[j] = ((a[j + 1] - a[j]) / h[j]) - ((h[j] * (c[j + 1] + 2 * c[j])) / 3);
            d[j] = (c[j + 1] - c[j]) / (3 * h[j]);
        }

        for (auto i = 0; i < n; ++i) {
            result.push_back({a[i], b[i], c[i], d[i], xvalues[i]});
        }

        return result;
    }

    template<typename Float>
    void to_matlab_format(const std::vector<cubic_piece<Float>>& pieces)
    {
        std::ofstream stream;
        stream.open("result.txt");

        stream << "[";
        for (auto i = pieces.begin(); i != pieces.end(); ++i)
        {
            stream << i->a << ' ' << i->b << ' ' << i->c << ' ' << i->d << ';';
        }
        stream << ']';

        stream.close();
    }
}
#endif //CSPLINE_SPLINE_HPP
