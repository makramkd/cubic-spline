#include <iostream>
#include <cmath>

#include "spline.hpp"

int main() {
    std::vector<double> x(11);
    std::vector<double> y(11);
    for (auto i = 0; i < x.size(); ++i) {
        x[i] = i;
        y[i] = std::sin(i);
    }

    auto spline = sp::spline(x, y);
    sp::to_matlab_format(spline);
}